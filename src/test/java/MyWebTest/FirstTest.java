package MyWebTest.stepdefinition;

//import Pages.LogApp;
import Pages.LoginPage;
import Pages.MailBoxPage;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;
//import java.util.logging.Logger;
import org.apache.log4j.Logger;


public class FirstTest {

        public static WebDriver driver;
        public static LoginPage loginPage;
        public static MailBoxPage mailBoxPage;
        public String writeTo = "TestTo1234567890@test.ru";
        public String writeAbout = "TestAbout1234567890";
        public String writeBody = "TestBody1234567890";
        private static Logger log = Logger.getLogger(FirstTest.class.getName());

        @BeforeClass
        public static void setup() {
            System.setProperty("webdriver.chrome.driver", "C:/Users/22437/IdeaProjects/chromedriver_win32/chromedriver.exe");
            driver = new ChromeDriver();
            loginPage = new LoginPage(driver);
            mailBoxPage = new MailBoxPage(driver);
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
            driver.get("https://gmail.com");
        }
            @Test
            public void userLogin() {
            try {
                LoginPage.inputLogin("testovm54@gmail.com");
            }
            catch (Exception e) {
                log.info("Ошибка при вводе логина");
            }
            finally {
                log.info("Этап ввода логина завершен");
            }


                try {
                    LoginPage.inputPassword("TestPass54!");
                }
                catch (Exception e) {
                    log.info("Ошибка при вводе пароля");
                }
                finally {
//                    LogApp.writeLog("Этап ввода пароля завершен");
                }

// Проверка входа
                String mailUser = mailBoxPage.getUserMail();
                try {
                    Assert.assertEquals("testovm54@gmail.com", mailUser);
                }
                catch (Exception e) {
                    log.info("Вход осуществен некорректно");
                }
                finally {
                    log.info("Проверка входа окончена");
                }
//      Заполняем
                try {
                    mailBoxPage.writeMail(writeTo, writeAbout, writeBody, false);
                }
                catch (Exception e) {
                    log.info("Ошибка при написании письма");
                }
                finally {
                    log.info("Этап написания письма завершен");
                }

//      Проверяем черновики
                mailBoxPage.testDraft(writeTo, writeAbout, writeBody);
//      Отправляем
                try {
                    mailBoxPage.writeMail(writeTo, writeAbout, writeBody, true);
                }
                catch (Exception e) {
                    log.info("Ошибка при отправлении письма");
                }
                finally {
                    log.info("Этап отправления письма завершен");
                }


//      Проверяем отправленные
                mailBoxPage.testSending(writeTo, writeAbout, writeBody);

        }
    @AfterClass
    public static void tearDown() {
        try {
            MailBoxPage.outLogin();
        }
        catch (Exception e) {
            log.info("Ошибка при выходе из учетной записи");
        }
        finally {
            log.info("Этап выхода из учетной записи завершен");
        }
        driver.quit();
    }
}
