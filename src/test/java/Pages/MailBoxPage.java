package Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MailBoxPage {
    public MailBoxPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    public static WebDriver driver;

    @FindBy(name = "to")
    private WebElement toField;

    @FindBy(name = "subjectbox")
    private WebElement aboutField;

    @FindBy(name = "to")
    private static WebElement writeToField;

    @FindBy(xpath = "//table/tbody/tr[1]/td/div/div/div[2]/div/div/table/tbody/tr/td/div[2]/div")
    private WebElement bodyField;

    @FindBy(xpath = "/html/body/div[7]/div[3]/div/div[1]/div[4]/header/div[2]/div[2]/div/form/div/input")
    private WebElement testToField;

    @FindBy(name = "subjectbox")
    private WebElement testAboutText;

    @FindBy(xpath = "//div/div/div/div/table/tbody/tr/td[2]/div[2]/div")
    private WebElement testBodyText;

    @FindBy(xpath = "//div/div/div/div/table/tbody/tr/td[2]/div[2]/div")
    private WebElement searchToField;

    @FindBy(xpath = "//div[4]/div[4]/div[1]/div/table/tbody/tr/td[6]/div/div/div/span/span")
    private WebElement sendAboutText;

    @FindBy(xpath = "//div[1]/div/div[4]/div[4]/div[1]/div/table/tbody/tr/td[6]/div/div/span")
    private WebElement sendBodyText;

    public String getUserMail() {
        driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[1]/div[4]/header/div[2]/div[3]/div/div[2]/div/a/span")).click();
        WebElement userLogin = driver.findElement(By.xpath("//html/body/div[7]/div[3]/div/div[1]/div[4]/header/div[2]/div[6]/div[1]/div/div[2]"));
        return userLogin.getText();
    }
    public void writeMail(String writeTo, String writeAbout, String writeBody, Boolean sending) {
        driver.findElement(By.xpath("//div[text()='Написать']")).click();
        writeToField.sendKeys(writeTo);
        aboutField.sendKeys(writeAbout);
        bodyField.sendKeys(writeBody);

        WebElement toField = driver.findElement(By.name("to"));
        toField.sendKeys("TestTo1234567890@test.ru");
        WebElement aboutField = driver.findElement(By.name("subjectbox"));
        aboutField.sendKeys("TestAbout1234567890");
        WebElement bodyField = driver.findElement(By.xpath("//div[4]/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/div/div[2]/div/div/table/tbody/tr/td/div[2]/div"));
        bodyField.sendKeys("TestBody1234567890");
//        if (sending) {
//            driver.findElement(By.xpath("//table/tbody/tr[2]/td/div/div/div[4]/table/tbody/tr/td/div/div[2]")).click();
//        }
    }
    public void testDraft(String writeTo, String writeAbout, String writeBody) {
        driver.findElement(By.xpath("//div[2]/div/div/div/div[2]/div/div[1]/div[1]/div/div[5]/div/div/div[2]/span/a")).click();
        driver.findElement(By.xpath("//div[2]/div/div[2]/div/div/div/div/table/tbody/tr/td[2]/img[3]")).click();
        testToField.sendKeys("                 TestTo1234567890@test.ru", Keys.ENTER);
//   WebElement testAboutText = driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[4]/div[4]/div[1]/div/table/tbody/tr/td[6]/div/div/div/span/span"));
        driver.findElement(By.xpath("//div/div[3]/div[1]/div/table/tbody/tr[1]/td[6]/div/div/span")).click();
        String testAbout = testAboutText.getText();
        Assert.assertEquals(writeAbout, testAbout);
        String testBody = testBodyText.getText();
        Assert.assertEquals(writeBody, testBody);
    }
    public void testSending(String writeTo, String writeAbout, String writeBody) {
        driver.findElement(By.xpath("//div[2]/div/div[1]/div[1]/div/div[4]/div/div/div[2]/span/a")).click();
        searchToField.sendKeys("TestTo1234567890@test.ru");
        String sendAbout = sendAboutText.getText();
        Assert.assertEquals("TestAbout1234567890", sendAbout);
        String sendBody = sendBodyText.getText();
        Assert.assertEquals("TestBody1234567890", sendBody);
    };

    public static void outLogin() {
        driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[1]/div[4]/header/div[2]/div[3]/div/div[2]/div/a/span")).click();
        driver.findElement(By.xpath("//*[@id=\"gb_71\"]")).click();
    }
}


