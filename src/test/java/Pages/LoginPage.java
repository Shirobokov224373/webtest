package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Level;

public class LoginPage {

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    public static WebDriver driver;

    @FindBy(id = "identifierId")
    public static WebElement loginField;

    @FindBy(name = "password")
    public static WebElement passwordField;

    public static void inputLogin(String login) {
        loginField.sendKeys(login, Keys.ENTER);

    }
    public static void inputPassword(String password) {
        passwordField.sendKeys(password, Keys.ENTER);
    }
}